const paragraf = document.querySelectorAll("p");
const links = document.querySelectorAll("a");
const container = document.querySelector("body");
const theme = document.querySelector(".theme");

const textArray = [...paragraf];
const linksArray =[...links];

function blakColor() {
    linksArray.forEach((e)=>{
        e.classList.add("theme");
    });
    container.classList.add("theme")
}

function whiteColor(){
    linksArray.forEach((e)=>{
        e.classList.remove("theme")
    });
    container.classList.remove("theme")
}

theme.addEventListener("click", (e)=> {
    e.preventDefault();
    if(localStorage.getItem("theme")==="black"){
        localStorage.removeItem("theme");
    } else {
localStorage.setItem("theme", "black")
    }
    haveBlackTheme()
})

function haveBlackTheme(){
    if(localStorage.getItem("theme")==="black"){
        blakColor();
    }else{
        whiteColor();
    }
}

haveBlackTheme();